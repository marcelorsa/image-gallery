# Image Gallery #

The Problem:

You got a request from a friend to create a gallery for his weeding where his friends will be able to upload their photos and he`ll have a unified gallery with all friend's photos.
He wants to be able to approve the the photos before be visible to everyone. He and his wife should be the only one able to approve new photos.
Users must be able to like photos.
Users should be able to sort the photos by total of likes or by date taken.

Please create a website that supply their needs. The photos must be saved on Amazon AWS S3 and the gallery must be fast to open even if there many photos.


### Details of the solution ###
* The resolution must be a web application.
* There must supply all the information needed to test the application
* The application must run
* The code needs to be hosted in your preferred code repository
* You need to host the application in a server of your choice and give us a link to access and use the application
* You should provide sufficient evidence that your solution is complete by, as a minimum, indicating that it works correctly against the requirements

### Work environment used to run this project: ###

* MacBook Pro 
* macOS High Sierra
* Visual Studio Code editor
* Python 3.7+
* Django 2.0
* Django Rest Frame Work 3.7.7
* Gunicorn 19.4.1
* Postgres 9.5

### Dependences ###

* Python 3.7+

Install Pyenv:
https://github.com/pyenv/pyenv#installation

Intall virtualenv for Pyenv:
https://github.com/pyenv/pyenv-virtualenv


Create and active a virtual environment.

```
pyenv install 3.7.0a3
pyenv virtualenv 3.7.0a3 venv
pyenv activate venv
```

Install requirements:
```
pip install -r requirements.txt
```


### Setup database 

* You need to create a databaes and user in PostgreSQL:

Create database: `createdb image_gallery_db`
Create user: `createuser -P -s image_gallery_admin`

* Create environment variables:
```
cp contrib/env-sample .env
```


### Run project on development environment:

* First of all run all migrations:
```
python manage.py migrate
```

* Run tests:
```
python manage.py test
```

* Create users:
```
python manage.py createsuperuser
```

* Run project:
```
python manage.py runserver
```

### Demo: ###
* Acess link to test demo: https://image-gallery-anchorloans.herokuapp.com/
* Users to approve images:
    * Username: john, password: john123456
    * Username: anna, password: anna123456