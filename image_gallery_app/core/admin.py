from django.contrib import admin
from image_gallery_app.core.models import ImageGallery


class ImageGalleryModelAdmin(admin.ModelAdmin):
    list_display = ('title', 'uploaded_at', 'status')

    actions = ['mark_as_approved']

    def mark_as_approved(self, request, queryset):
        count = queryset.update(status='ok')

        if count == 1:
            msg = '{} Image was approved!'
        else:
            msg = '{} Images was approved!'

        self.message_user(request, msg.format(count))

    mark_as_approved.short_description = 'mark as approved'


admin.site.register(ImageGallery, ImageGalleryModelAdmin)
