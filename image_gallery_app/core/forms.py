from django import forms
from image_gallery_app.core.models import ImageGallery


class UploadImageForm(forms.ModelForm):
    class Meta:
        model = ImageGallery
        fields = ('image', 'title')
