from django.db import models
from django.db.models import F


class ImageGallery(models.Model):
    UPLOADED = 'up'
    APPROVED = 'ok'
    STATUS_CHOICES = (
        (UPLOADED, 'uploaded'),
        (APPROVED, 'approved'),
    )
    image = models.ImageField(upload_to='weeding_images/')
    title = models.CharField(max_length=20, blank=True)
    likes = models.IntegerField(default=0)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=2,
                              choices=STATUS_CHOICES,
                              default=UPLOADED)

    class Meta:
        ordering = ['uploaded_at', 'likes']

    def __str__(self):
        return self.title

    def increse_likes(self):
        self.likes = F('likes') + 1
        self.save()
        self.refresh_from_db()
