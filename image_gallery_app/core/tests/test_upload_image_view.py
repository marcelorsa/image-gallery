from django.test import TestCase, override_settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.shortcuts import reverse as r
from image_gallery_app.core.forms import UploadImageForm
from image_gallery_app.core.models import ImageGallery

TINY_GIF = b'GIF89a\x01\x00\x01\x00\x00\xff\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x00;'


class UpaloadImageGet(TestCase):
    def setUp(self):
        self.response = self.client.get(r('upload_image'))

    def test_get(self):
        """Get upload_image/ must return status code 200"""
        self.assertEqual(200, self.response.status_code)

    def test_template(self):
        """Must use upload_image.html"""
        self.assertTemplateUsed(self.response, 'upload_image.html')

    def test_html(self):
        """HTML must contain inputs tags"""
        tags = (('<input', 4),
                ('<button', 1),
                ('type="file"', 1),
                ('type="text"', 2),
                ('type="submit"', 1))
        for text, count in tags:
            with self.subTest():
                self.assertContains(self.response, text, count)

    def test_csrf(self):
        """HTML contains csrf"""
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_has_form(self):
        """Context must have subscritption_form"""
        form = self.response.context['form']
        self.assertIsInstance(form, UploadImageForm)


@override_settings(DEFAULT_FILE_STORAGE='inmemorystorage.InMemoryStorage')
class UpaloadImagePostValid(TestCase):
    def setUp(self):
        self.image_file = SimpleUploadedFile('tiny.gif', TINY_GIF)
        data = {'image': self.image_file, 'title': 'tiny gif'}
        self.response = self.client.post(r('upload_image'), data=data,
                                         follow=True)

    def test_post_valid_form(self):
        """ POST upload_image valid form """
        self.assertRedirects(self.response, r('home'))

    def test_save_image(self):
        self.assertTrue(ImageGallery.objects.exists())


class UpaloadImagePostInvalid(TestCase):
    def setUp(self):
        self.response = self.client.post(r('upload_image'), data={})
    
    def test_post(self):
        """Invalid POST should not redirect"""
        self.assertEqual(200, self.response.status_code)
    
    def test_template(self):
        """Must use upload_image.html"""
        self.assertTemplateUsed(self.response, 'upload_image.html')
    
    def test_has_form(self):
        form = self.response.context['form']
        self.assertIsInstance(form, UploadImageForm)
    
    def test_form_has_errors(self):
        form = self.response.context['form']
        self.assertTrue(form.errors)

    def test_dont_save_subscription(self):
        self.assertFalse(ImageGallery.objects.exists())
