from django.test import TestCase, override_settings
from django.core.files.uploadedfile import SimpleUploadedFile
from image_gallery_app.core.forms import UploadImageForm

TINY_GIF = b'GIF89a\x01\x00\x01\x00\x00\xff\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x00;'


@override_settings(DEFAULT_FILE_STORAGE='inmemorystorage.InMemoryStorage')
class UpaloadImageFormTest(TestCase):

    def test_form_fields(self):
        """Form must have one field"""
        form = UploadImageForm()
        expect = ['image', 'title']
        self.assertSequenceEqual(expect, list(form.fields))

    def test_valid_form(self):
        image_file = SimpleUploadedFile('tiny.gif', TINY_GIF)
        data = dict(image=image_file)
        form = UploadImageForm({'title': 'tiny gif'}, data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form = UploadImageForm({}, {})
        self.assertFalse(form.is_valid())
    
    def test_form_has_error(self):
        form = UploadImageForm({}, {})
        self.assertTrue(form.errors)