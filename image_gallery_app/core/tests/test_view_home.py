
from django.test import TestCase, override_settings
from django.urls import reverse as r
from django.core.files.uploadedfile import SimpleUploadedFile
from image_gallery_app.core.models import ImageGallery


TINY_GIF = b'GIF89a\x01\x00\x01\x00\x00\xff\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x00;'


class Home(TestCase):
    def setUp(self):
        self.response = self.client.get(r('home'))

    def test_get(self):
        """Get / must return status code 200"""
        self.assertEqual(200, self.response.status_code)

    def test_template(self):
        """Must use home.html"""
        self.assertTemplateUsed(self.response, 'home.html')

    def test_upload_image_link(self):
        """ Must contains link of upload image page """
        expected = 'href="{}"'.format(r('upload_image'))
        self.assertContains(self.response, expected)

    def test_image_gallery_link(self):
        """ Must contains link of image gallery page """
        expected = 'href="{}"'.format(r('home'))
        self.assertContains(self.response, expected)

    def test_login_link(self):
        """ Must contains link of login page """
        expected = 'href="{}"'.format(r('admin:index'))
        self.assertContains(self.response, expected)


@override_settings(DEFAULT_FILE_STORAGE='inmemorystorage.InMemoryStorage')
class ImageListGet(TestCase):
    def setUp(self):
        self.image_file = SimpleUploadedFile('tiny.gif', TINY_GIF)
        ImageGallery.objects.create(image=self.image_file,
                                    title='A tiny gif',
                                    status='ok')
        ImageGallery.objects.create(image=self.image_file,
                                    title='A tiny gif',
                                    status='ok')
        ImageGallery.objects.create(image=self.image_file,
                                    title='A tiny gif',
                                    status='ok')

        self.response = self.client.get(r('home'))

    def test_get(self):
        """Get / must return status code 200"""
        self.assertEqual(200, self.response.status_code)

    def test_template(self):
        """Must use image_gallery.html"""
        self.assertTemplateUsed(self.response, 'image_gallery.html')

    def test_context(self):
        variables = ['imagegallery_list', 'images']

        for key in variables:
            with self.subTest():
                self.assertIn(key, self.response.context)


class ImageListEmpty(TestCase):
    def test_has_not_image(self):
        """Must render Not image yet text """
        response = self.client.get(r('home'))
        self.assertContains(response, 'Not images yet!')


@override_settings(DEFAULT_FILE_STORAGE='inmemorystorage.InMemoryStorage')
class ImageLikesPost(TestCase):
    def setUp(self):
        self.image_file = SimpleUploadedFile('tiny.gif', TINY_GIF)
        ImageGallery.objects.create(image=self.image_file,
                                    title='A tiny gif',
                                    status='ok')
        self.id = ImageGallery.objects.first().id
        url = r('like', args=[self.id])
        self.response = self.client.post(path=url,
                                         HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                         follow=True)

    def test_post_invalid_image_id(self):
        """ Post /like/<image_id> with invalid id must return
            status code 404
        """
        any_id = 33
        url = r('like', args=[any_id])
        response = self.client.post(path=url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    follow=True)
        self.assertEqual(404, response.status_code)

    def test_post_without_image_id(self):
        """ Post /like/ without id must return status code 404 """
        response = self.client.post(path='/like/',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    follow=True)
        self.assertEqual(404, response.status_code)

    def test_post_no_ajax(self):
        """ Post no ajax /like/<image_id> must return status code 404 """
        any_id = 33
        url = r('like', args=[any_id])
        response = self.client.post(path=url,
                                    follow=True)
        self.assertEqual(400, response.status_code)

    def test_post_valid_image_id(self):
        """ Post /like/<image_id> must return status code 200"""
        self.assertEqual(200, self.response.status_code)

    def test_json(self):
        """ The response of AJAX request must contain json data as dict """
        exected = dict(likes=1)
        self.assertEqual(exected, self.response.json())


@override_settings(DEFAULT_FILE_STORAGE='inmemorystorage.InMemoryStorage')
class SortImages(TestCase):
    def setUp(self):
        self.image_file = SimpleUploadedFile('tiny.gif', TINY_GIF)
        ImageGallery.objects.create(image=self.image_file,
                                    title='A tiny gif 1',
                                    status='ok')
        ImageGallery.objects.create(image=self.image_file,
                                    title='A tiny gif 2',
                                    status='ok', likes=1)
        ImageGallery.objects.create(image=self.image_file,
                                    title='A tiny gif 3',
                                    status='ok', likes=3)
        self.resp_likes = self.client.get(path='/?sort_by=likes')
        self.resp_upload_at = self.client.get(path='/?sort_by=upload_at')

    def test_sort_by_descending_likes(self):
        """ Must be return list of descending likes """
        expect_likes = [3, 1, 0]
        images = self.resp_likes.context.get('imagegallery_list')
        result_likes = [image.likes for image in images]
        self.assertEqual(200, self.resp_likes.status_code)
        self.assertListEqual(expect_likes, result_likes)

    def test_sort_by_descending_upload_at(self):
        """ Must be return list of descending uploaded images """
        expected = [
            'A tiny gif 1',
            'A tiny gif 2',
            'A tiny gif 3'
        ]
        images = self.resp_upload_at.context.get('imagegallery_list')
        result = [image.title for image in images]
        self.assertEqual(200, self.resp_likes.status_code)
        self.assertListEqual(expected, result)
