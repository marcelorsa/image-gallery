from django.test import TestCase, override_settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.utils import DataError
from image_gallery_app.core.models import ImageGallery


TINY_GIF = b'GIF89a\x01\x00\x01\x00\x00\xff\x00,\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x00;'

@override_settings(DEFAULT_FILE_STORAGE='inmemorystorage.InMemoryStorage')
class ImageGalleryTesteModel(TestCase):

    def setUp(self):
        self.image_file = SimpleUploadedFile('tiny.gif', TINY_GIF)
        ImageGallery.objects.create(image=self.image_file, title='A tiny gif')

    def test_has_fields(self):
        fields = (
            'id',
            'image',
            'title',
            'likes',
            'uploaded_at',
            'status',
        )
        model_fields = ImageGallery._meta.fields
        for field in model_fields:
            has_field = field.name in fields
            with self.subTest():
                self.assertTrue(has_field)
            
    def test_valid_upload_image(self):
        self.assertTrue(ImageGallery.objects.exists())
    
    def test_invalid_status(self):
        with self.assertRaisesMessage(DataError, 'value too'
                                      ' long for type'
                                      ' character varying(2)'):
                ImageGallery.objects.create(image=self.image_file,
                                            status='updated')

    def test_valid_update_likes(self):
        expect_likes = 2
        image = ImageGallery.objects.first()
        image.increse_likes()
        image.increse_likes()
        self.assertEqual(expect_likes, image.likes)
