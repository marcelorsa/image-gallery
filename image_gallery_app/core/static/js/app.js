(function ($) {
  
    $( ".like" ).click(function() {sendLike(this); });
  
})(jQuery);
  
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sendLike(elm) {
    var csrftoken = getCookie('csrftoken');
    var id = $(elm).data('image-id');
    var url = "/like/" + id + "/"

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post( url, function() {})
        .done(function(data) {
            elm.text = "Like (" + data.likes + ")"
        })
        .fail(function() {
          alert( "Error occur: It was not possible like thi phonto" );
        })
}