from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.views import View
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from image_gallery_app.core.forms import UploadImageForm
from image_gallery_app.core.models import ImageGallery


class ImagesListView(ListView):
    model = ImageGallery
    filter_fields = ('uploaded_at', 'likes')
    template_name = 'home.html'
    paginate_by = 5

    def get_queryset(self):
        query_string = self.request.GET.get('sort_by')
        if query_string in self.filter_fields:
            return self.model.objects.filter(status='ok').order_by(f'-{query_string}')
        return self.model.objects.filter(status='ok')


class LikeView(View):

    def post(self, request, *args, **kwargs):
        if not self.request.is_ajax():
            error = dict(error='Request is not AJAX.')
            return JsonResponse(error, status=400)

        image_id = kwargs['id']
        image = get_object_or_404(ImageGallery, pk=image_id)
        image.increse_likes()
        return JsonResponse({'likes': image.likes})


like = LikeView.as_view()
upload_image = CreateView.as_view(template_name='upload_image.html',
                                  form_class=UploadImageForm,
                                  success_url='/')
home = ImagesListView.as_view()
