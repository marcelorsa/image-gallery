from django.urls import path
from image_gallery_app.core.views import home, upload_image, like

urlpatterns = [
    path('', home, name='home'),
    path('upload_image/', upload_image, name='upload_image'),
    path('like/<int:id>/', like, name='like'),
]